# groovy-docker

Integration of Docker client library to run Docker from a Groovy environment. The example.groovy file within src/main/groovy interacts with the host machine Docker daemon to run docker command from a groovy environment.

## Setup project

You will need JDK installed in the machine as a prerequisite. Then run the setup script.

Install the needed by running:
```sh
setup/install_required_sw.sh
```

## Build & Run

The gradle tasks that you will need to execute in order to get dependencies, compile,
and run are the following:
```sh
$ ./gradlew getDeps build runScript
```
Dependencies are downloaded to the 'runtime' folder. If you want to run the example from command line instead of using the gradle tasks you can:

```sh
$ groovy -cp "runtime/*" src/main/groovy/example.groovy
```

## TODO

* TODO: Encapsulate the application in a container. The automation to build the container can be done using gradle tasks or just using docker build with the Dockerfile

* TODO: Once the example script runs within a docker container, enhance the example script. The new script shall run an alpine container using the Docker client library and output the logs of the execution dinamically. In order to appreciate the logs being outputed to stdout, the alpine execution shall act as a timer, in which each second a monotonically incremental number is output. As an example, the output logs should be:
```sh
[INFO]: Logs from Docker cilent library
....
[INFO]: Executing the increasing counter in Alpine container.
0
1
2
3
4
...
[INFO]: Any extra Docker client library logs
```
Once the counter reaches 20 it shall end and the execution of the groovy script shall end

[Info on the Docker client library](https://github.com/gesellix/docker-client)
