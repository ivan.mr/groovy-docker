import de.gesellix.docker.client.DockerClientImpl

def dockerClient = new DockerClientImpl("unix:///var/run/docker.sock")
def info = dockerClient.info().content

println(info)
